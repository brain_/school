<?php

class Needleman {
    private static $arrow_up = '&#11014;';
    private static $arrow_left = '&#11013;';
    private static $arrow_nw = '&#11017;';

    private $match_score = 1;
    private $mismatch_score = 0;
    private $gap_penalty = -1;

    private $matrix = array();
    private $optimal_alignment = array();

    /**
     * Constructor
     */
    public function __construct($match_score, $mismatch_score, $gap_penalty) {
        $this->match_score = $match_score;
        $this->mismatch_score = $mismatch_score;
        $this->gap_penalty = $gap_penalty;
    }

    /**
     * Calculeaza alinierea globala a algoritmului Needleman-Wunsch si returneaza structura reprezentata in tabel.
     */
    public function compute($first_sequence, $second_sequence) {
        $this->init($first_sequence, $second_sequence);

        for($i = 1; $i < count($this->matrix); $i++) {
            for($j = 1; $j < count($this->matrix[$i]); $j++) {
                $match_mismatch = ($first_sequence[$i-1] === $second_sequence[$j-1]) ? $this->match_score : $this->mismatch_score;
                $match = $this->matrix[$i-1][$j-1]['val'] + $match_mismatch;
                $hgap = $this->matrix[$i-1][$j]['val'] + $this->gap_penalty;
                $vgap = $this->matrix[$i][$j-1]['val'] + $this->gap_penalty;
                $max = max($match, $hgap, $vgap);
                $arrow = self::$arrow_nw;
                if($max === $hgap) {
                    $arrow = self::$arrow_up;
                } else if($max === $vgap) {
                    $arrow = self::$arrow_left;
                }

                $this->matrix[$i][$j]['arrow'] = $arrow;
                $this->matrix[$i][$j]['val'] = $max;
            }
        }

        $i = count($this->matrix)-1;
        $j = count($this->matrix[0])-1;

        $this->optimal_alignment['first_sequence'] = array();
        $this->optimal_alignment['second_sequence'] = array();
        $this->optimal_alignment['aln'] = array();
        $this->optimal_alignment['score'] = $this->matrix[$i][$j]['val'];

        while($i !== 0 and $j !== 0) {
            $base1 = $first_sequence[$i-1];
            $base2 = $second_sequence[$j-1];
            $this->matrix[$i][$j]['trace'] = true;
            $arrow = $this->matrix[$i][$j]['arrow'];


            if($arrow === self::$arrow_nw) {
                $i--;
                $j--;
                $this->optimal_alignment['first_sequence'][] = $base1;
                $this->optimal_alignment['second_sequence'][] = $base2;
                $this->optimal_alignment['aln'][] = ($base1 === $base2) ? '|' : ' ';
            } else if($arrow === self::$arrow_up) {
                $i--;
                $this->optimal_alignment['first_sequence'][] = $base1;
                $this->optimal_alignment['second_sequence'][] = '-';
                $this->optimal_alignment['aln'][] = ' ';
            } else if($arrow === self::$arrow_left) {
                $j--;
                $this->optimal_alignment['first_sequence'][] = '-';
                $this->optimal_alignment['second_sequence'][] = $base2;
                $this->optimal_alignment['aln'][] = ' ';
            } else {
                die("Invalid arrow: $i,$j");
            }
        }

        foreach(array('first_sequence', 'second_sequence', 'aln') as $k) {
            $this->optimal_alignment[$k] = array_reverse($this->optimal_alignment[$k]);
        }
        return $this->matrix;
    }

    /**
     * Returneaza alinierea optima
     */
    public function getOptimalGlobalAlignment() {
        return $this->optimal_alignment;
    }

    /**
     * Evalueaza alinierea globala si afiseaza rezultatul in HTML
     */
    public function renderAsHTML($first_sequence, $second_sequence, $full_page=true) {
        $this->compute($first_sequence, $second_sequence);

        if($full_page) {
            echo '<title>Needleman-Wunsch Alignment Score Table</title>';
            echo '<style type="text/css">';
            echo '.trace { background-color: #c99;font-weight: bold }';
            echo '.seq { background-color: #ccc;}';
            echo '.data { border-collapse: collapse }';
            echo '.data td { border: 1px solid #666; text-align: center; }';
            echo '.align td { text-align: center; }';
            echo '</style>';
            echo '</head>';
            echo '<body>';
        }
        echo '<h3 align="center">Tabela de Aliniere</h3>';
        echo '<table class="data"; align="center">';
        echo '<tr><td>&nbsp;</td><td>&nbsp;</td>';
        for($i = 0; $i < strlen($second_sequence); $i++) {
            echo '<td class="seq">'.$second_sequence[$i].'</td>';
        }

        echo '</tr>';
        for($i = 0; $i < count($this->matrix); $i++) {
            echo "<tr>\n";
            if($i > 0) {
                echo '<td class="seq">'.$first_sequence[$i-1].'</td>';
            } else  {
                echo '<td>&nbsp;</td>';
            }

            foreach($this->matrix[$i] as $r) {
                $str = '<td';
                $str .= $r['trace'] ? ' class="trace">' : '>';
                $str .= ($r['arrow'] !== null) ? $r['arrow'] : '&nbsp;';
                $str .= '&nbsp;';
                $str .= $r['val'] < 0 ? $r['val'] : '&nbsp;'.$r['val'];
                $str .= '</td>';
                echo $str;
            }

            echo "</tr>\n";
        }
        echo "\n</table>";
        echo '<h3 align="center">Aliniere optima (rezultat = '.$this->optimal_alignment['score'].')</h3>';
        echo '<table class="align"; align="center">';
        foreach(array('second_sequence', 'aln', 'first_sequence') as $k) {
            echo '<tr>';
            foreach($this->optimal_alignment[$k] as $v) {
                echo "<td>$v</td>";
            }
            echo '</tr>';
        }
        echo "\n</table>";

        if($full_page) echo '</body></html>';
    }

    public function renderOnScreen($first_sequence, $second_sequence) {
        $this->compute($first_sequence, $second_sequence);

        echo "Alignment Score Table\n\n";

        $char_array = array();
        for($i = 0; $i < strlen($second_sequence); $i++) {
            $char_array[] = '   '.$second_sequence[$i];
        }
        echo "\t\t".implode("\t", $char_array)."\n";
        for($i = 0; $i < count($this->matrix); $i++) {
            if($i > 0) {
                echo $first_sequence[$i-1];
            } else  {
                echo ' ';
            }
            echo "\t";

            $char_array = array();
            foreach($this->matrix[$i] as $r) {
                $str = ($r['arrow'] !== null) ? html_entity_decode($r['arrow'], ENT_QUOTES, 'UTF-8') : ' ';
                $str .= ' ';
                $str .= $r['val'] < 0 ? $r['val'] : ' '.$r['val'];
                $str .= $r['trace'] ? '*' : ' ';
                $char_array[] = $str;
            }
            echo implode("\t", $char_array);
            echo "\n";
        }

        echo "\nOptimal Alignment (score = ".$this->optimal_alignment['score'].")\n";
        foreach(array('second_sequence', 'aln', 'first_sequence') as $k) {
            echo implode(' ', $this->optimal_alignment[$k])."\n";
        }
    }

    /**
     * Initializare
     */
    private function init($first_sequence, $second_sequence) {
        $this->matrix = array();
        $this->optimal_alignment = array();
        for($i = 0; $i < strlen($first_sequence)+1; $i++) {
            for($j = 0; $j < strlen($second_sequence)+1; $j++) {
                $this->matrix[$i][$j] = array(
                    'arrow' => null,
                    'trace' => null,
                    'val' => 0
                );
            }
        }

        for($i = 0; $i < strlen($first_sequence); $i++) {
            $this->matrix[$i+1][0]['val'] = ($i+1) * $this->gap_penalty;
        }

        for($j = 0; $j < strlen($second_sequence); $j++) {
            $this->matrix[0][$j+1]['val'] = ($j+1) * $this->gap_penalty;
        }
    }
}

?>
