Algoritmul Needleman–Wunsch este un algoritm folosit in bioinformatica pentru a alinia secvential proteine sau nucleotide. A fost printre primele aplicatii ale programarii dinamice in cea ce priveste compararea secventierii biologice. Algoritmul a fost dezvoltat de catre Saul B. Needleman si Christian D. Wunsch si publicat in anul 1970.
Cum functioneza algoritmul: Imparte o secventa (problema) mare in secvente mai mici si foloseste solutia de la problemele mai mici pentru a gasi solutia optima la problema mai mare.Algoritmul asigneaza un scor fiecarui posibil aliniament  si apoi incearca sa gaseasca aliniamentele cu cel mai mare scor. Alinierea se face conform unei matrici de compensare. Pentru o operatie corecta, valoarea metodei de potrivire trebuie sa fie corecta iar celelalte valori sa fie negative.

Pentru aplicatia “Algoritm Needleman-Wunsch” am creat o interfata prietenoasa pentru a fi cat mai usor de utilizat.
Pentru a folosi aplicatia puteti accesa link-ul de mai jos sau copiati link-ul in browser-ul dumneavoastra:
http://nw.joyoclock.ro

Codul sursa poata fi descarcat de la aceasta adresa : 
https://bitbucket.org/brain_/school/src/master/





In aplicatia noastra sunt disponibile urmatoarele optiuni ( in partea din stanga sus a ecranului dumneavoastra ) :

Potrivire : cand cele 2 caractere se potrivesc ( sunt identice ). In acest camp trebuie introduse caractere numerice.
 

Nepotrivire: cand cele 2  caractere nu se potrivesc ( sunt diferite ). In acest camp trebuiesc introduse caractere numerice.
 

Gap: acest camp il folosim pentru a permite o anumita lipsa de aliniament astfel putem gasi mai multe match-uri. In acest camp trebuiesc introduse caractere numerice.
 

Secventa 1: acesta optiune deserveste prima secventa pe care dorim sa o introducem in aplicatie. In acesta celula putem introduce caractere alfabetice.
 

Secventa 2: acesta optiune deserveste a doua secventa pe care dorim sa o introducem in aplicatie. In acesta celula putem introduce caractere alfabetice.
 


Dupa ce datele au fost introduse in campurile mai sus mentionate, se acceseaza butonul “Calculeaza”.



Pentru a rula aplicatia local este nevoie de orice php editor ( ex. Brackets / PHPStorm etc. ).
http://brackets.io/ 
https://www.jetbrains.com/phpstorm/download/

 
