<?php

require_once('needleman-class.php');

$match = isset($_GET['match']) ? $_GET['match'] : 3;
$mismatch = isset($_GET['mis']) ? $_GET['mis'] : -1;
$gap = isset($_GET['gap']) ? $_GET['gap'] : -2;
$first_sequence = isset($_GET['first_sequence']) ? $_GET['first_sequence'] : 'TTACG';
$second_sequence = isset($_GET['second_sequence']) ? $_GET['second_sequence'] : 'ATAAA';

/**
 * Validare pe match/mismatch/gap in cazul in care user-ul introduce alte valori decat integer, sa se puna valorile default
 */
if(!is_numeric($match)) $match = 3;
if(!is_numeric($mismatch)) $mismatch = -1;
if(!is_numeric($gap)) $gap = -2;

/**
 * Validare pe secvente in cazul in care user-ul introduce alte valori decat string, sa se puna valorile default
 */
if (is_integer($first_sequence)) $first_sequence= 'TTACG';
if (is_integer($second_sequence)) $second_sequence = 'ATAAA';

/**
 * Validare pe secvente in cazul in care user-ul nu introduce nici o valoare, sa se puna valorile default
 */
if(empty($first_sequence)) $first_sequence = 'TTACG';
if(empty($second_sequence)) $second_sequence = 'ATAAA';

if(strlen($first_sequence) > 15) $first_sequence = substr($first_sequence, 0, 25);
if(strlen($second_sequence) > 15) $second_sequence = substr($second_sequence, 0, 25);

$nw = new Needleman($match, $mismatch, $gap);

?>
<!DOCTYPE html PUBLIC>
<head>
    <title>Needleman-Wunsch</title>
    <style type="text/css">
    .trace { background-color: #0F0;font-weight: bold }
    .seq { background-color: #FFF;}
    .data { border-collapse: collapse }
    .data td { border: 1px solid #D8D8D8; text-align: center; }
    .align td { text-align: center; }
    .config { border-collapse: collapse }
    .config td { font-size:small;border: 1px solid #ffffcc; text-align: left; padding: 5px;}
    </style>
</head>
<body>
<h4 align="center">Algoritm Needleman-Wunsch</h4>
<form method="get">

    <td>Potrivire: </td><td><input type="text" size="2" name="match" value="<?php echo htmlentities($match);?>" /></td>
    <td>Nepotrivire: </td><td><input type="text" size="2" name="mis" value="<?php echo htmlentities($mismatch);?>" /></td>
    <td>Gap: </td><td><input type="text" size="2" name="gap" value="<?php echo htmlentities($gap);?>"/></td> <br/>
    <td>Secventa 1: </td><td colspan="6"><input type="text" name="first_sequence" size="15" value="<?php echo htmlentities($first_sequence);?>"/></td>
    <td>Secventa 2: </td><td colspan="6"><input type="text" name="second_sequence" size="15" value="<?php echo htmlentities($second_sequence);?>" /></td>
    <td colspan="6"><input type="submit" value="Calculeaza"/></td>

</form>
<?php $nw->renderAsHTML($first_sequence, $second_sequence, false); ?>
</body>
</html>
